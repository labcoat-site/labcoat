# LabCOAT #



### What is LabCOAT? ###

**LabCOAT** (**Lab**oratory **C**oncepts **O**rganized **A**s **T**echnology) is a scientific research & development startup 
that provides curated services (APIs, data feeds, experiment verification, NFTs) and an automated IoT AI/ML data framework 
for lab equipment, manufacturing, and supply chains within an **XR** (e**X**tended **R**eality) environment.